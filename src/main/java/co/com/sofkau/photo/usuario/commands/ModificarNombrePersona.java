package co.com.sofkau.photo.usuario.commands;

import co.com.sofka.domain.generic.Command;
import co.com.sofkau.photo.usuario.values.Nombre;
import co.com.sofkau.photo.usuario.values.PersonaId;
import co.com.sofkau.photo.usuario.values.UsuarioId;

public class ModificarNombrePersona implements Command {
    private final UsuarioId usuarioId;
    private final PersonaId entityId;
    private final Nombre name;

    public ModificarNombrePersona(UsuarioId usuarioId, PersonaId entityId, Nombre name){
        this.usuarioId = usuarioId;
        this.entityId = entityId;
        this.name = name;
    }

    public UsuarioId getUsuarioId() {
        return usuarioId;
    }

    public PersonaId getEntityId() {
        return entityId;
    }

    public Nombre getName() {
        return name;
    }
}
