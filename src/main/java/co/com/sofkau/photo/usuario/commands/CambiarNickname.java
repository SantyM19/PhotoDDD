package co.com.sofkau.photo.usuario.commands;

import co.com.sofka.domain.generic.Command;
import co.com.sofkau.photo.usuario.values.NickName;
import co.com.sofkau.photo.usuario.values.UsuarioId;

public class CambiarNickname implements Command {
    private final UsuarioId usuarioId;
    private final NickName nickName;

    public CambiarNickname(UsuarioId usuarioId, NickName nickName) {
        this.usuarioId = usuarioId;
        this.nickName = nickName;
    }

    public UsuarioId getUsuarioId() {
        return usuarioId;
    }

    public NickName getNickName() {
        return nickName;
    }
}
