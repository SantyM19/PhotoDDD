package co.com.sofkau.photo.usuario.events;

import co.com.sofka.domain.generic.DomainEvent;
import co.com.sofkau.photo.usuario.values.NickName;

public class NickNameCambiado extends DomainEvent {
    private NickName nickName;

    public NickNameCambiado(NickName nickName) {
        super("sofka.usuario.nicknamecambiado");
        this.nickName = nickName;
    }

    public NickName nickName() {
        return nickName;
    }
}
