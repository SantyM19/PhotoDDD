package co.com.sofkau.photo.usuario.commands;

import co.com.sofka.domain.generic.Command;
import co.com.sofkau.photo.usuario.values.CuentaId;
import co.com.sofkau.photo.usuario.values.Edad;
import co.com.sofkau.photo.usuario.values.UsuarioId;

public class ModificarEmailCuenta implements Command {
    private final UsuarioId usuarioId;
    private final CuentaId entityId;
    private final Edad edad;

    public ModificarEmailCuenta(UsuarioId usuarioId, CuentaId entityId, Edad edad) {
        this.usuarioId = usuarioId;
        this.entityId = entityId;
        this.edad = edad;
    }

    public UsuarioId getUsuarioId() {
        return usuarioId;
    }

    public CuentaId getEntityId() {
        return entityId;
    }

    public Edad getEdad() {
        return edad;
    }
}
