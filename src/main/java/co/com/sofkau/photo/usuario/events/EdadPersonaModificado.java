package co.com.sofkau.photo.usuario.events;

import co.com.sofka.domain.generic.DomainEvent;
import co.com.sofkau.photo.usuario.values.Edad;
import co.com.sofkau.photo.usuario.values.PersonaId;

public class EdadPersonaModificado extends DomainEvent {
    private PersonaId entityId;
    private Edad edad;

    public EdadPersonaModificado(PersonaId entityId, Edad edad) {
        super("sofka.usuario.edadpersonamodificado");
        this.edad = edad;
        this.entityId = entityId;
    }

    public PersonaId entityId() {
        return entityId;
    }

    public Edad edad() {
        return edad;
    }
}
