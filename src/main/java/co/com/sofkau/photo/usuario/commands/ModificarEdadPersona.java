package co.com.sofkau.photo.usuario.commands;

import co.com.sofka.domain.generic.Command;
import co.com.sofkau.photo.usuario.values.Edad;
import co.com.sofkau.photo.usuario.values.PersonaId;
import co.com.sofkau.photo.usuario.values.UsuarioId;

public class ModificarEdadPersona implements Command {
    private final UsuarioId usuarioId;
    private final PersonaId entityId;
    private final Edad edad;

    public ModificarEdadPersona(UsuarioId usuarioId, PersonaId entityId, Edad edad) {
        this.usuarioId = usuarioId;
        this.entityId = entityId;
        this.edad = edad;
    }

    public UsuarioId getUsuarioId() {
        return usuarioId;
    }

    public PersonaId getEntityId() {
        return entityId;
    }

    public Edad getEdad() {
        return edad;
    }
}
