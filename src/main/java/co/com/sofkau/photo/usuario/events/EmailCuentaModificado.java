package co.com.sofkau.photo.usuario.events;

import co.com.sofka.domain.generic.DomainEvent;
import co.com.sofkau.photo.usuario.values.CuentaId;
import co.com.sofkau.photo.usuario.values.Email;

public class EmailCuentaModificado extends DomainEvent {

    private CuentaId entityId;
    private Email email;

    public EmailCuentaModificado(CuentaId entityId, Email email) {
        super("sofka.usuario.emailcuentamodificado");
        this.entityId = entityId;
        this.email = email;
    }

    public CuentaId entityId() {
        return entityId;
    }

    public Email email() {
        return email;
    }
}
