package co.com.sofkau.photo.usuario;

import co.com.sofka.domain.generic.Entity;
import co.com.sofkau.photo.usuario.values.CuentaId;
import co.com.sofkau.photo.usuario.values.Email;

import java.util.Objects;

public class Cuenta extends Entity<CuentaId> {
    private Email email;

    public Cuenta(CuentaId entityId, Email email) {
        super(entityId);
        this.email = email;
    }

    public Email email() {
        return email;
    }

    public void modificaEmail(Email email){
        this.email = Objects.requireNonNull(email);
    }
}
