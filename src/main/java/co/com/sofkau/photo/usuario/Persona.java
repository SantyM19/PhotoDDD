package co.com.sofkau.photo.usuario;

import co.com.sofka.domain.generic.Entity;
import co.com.sofkau.photo.usuario.values.Edad;
import co.com.sofkau.photo.usuario.values.Nombre;
import co.com.sofkau.photo.usuario.values.PersonaId;

import java.util.Objects;

public class Persona extends Entity<PersonaId> {
    private Nombre nombre;
    private Edad edad;

    public Persona(PersonaId entityId, Nombre nombre, Edad edad) {
        super(entityId);
        this.nombre = nombre;
        this.edad = edad;
    }

    public Nombre nombre() {
        return nombre;
    }

    public Edad edad() {
        return edad;
    }

    public void modificaNombre(Nombre nombre){
        this.nombre = Objects.requireNonNull(nombre);
    }
    public void modificarEdad(Edad edad){
        this.edad = Objects.requireNonNull(edad);
    }
}
