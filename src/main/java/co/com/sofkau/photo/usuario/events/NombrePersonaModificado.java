package co.com.sofkau.photo.usuario.events;

import co.com.sofka.domain.generic.DomainEvent;
import co.com.sofkau.photo.usuario.values.Nombre;
import co.com.sofkau.photo.usuario.values.PersonaId;

public class NombrePersonaModificado extends DomainEvent {
    private PersonaId entityId;
    private Nombre name;

    public NombrePersonaModificado(PersonaId entityId, Nombre name) {
        super("sofka.usuario.nombrepersonamodificado");
        this.name = name;
        this.entityId = entityId;
    }

    public PersonaId entityId() {
        return entityId;
    }

    public Nombre name() {
        return name;
    }
}
