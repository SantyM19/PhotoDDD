package co.com.sofkau.photo.usuario.events;

import co.com.sofka.domain.generic.DomainEvent;
import co.com.sofkau.photo.usuario.Cuenta;
import co.com.sofkau.photo.usuario.Persona;
import co.com.sofkau.photo.usuario.values.NickName;

public class UsuarioCreado extends DomainEvent {
    private final Persona persona;
    private final Cuenta cuenta;
    private final NickName nickName;

    public UsuarioCreado(Persona persona, Cuenta cuenta, NickName nickName) {
        super("sofka.usuario.usuariocreado");
        this.persona = persona;
        this.cuenta = cuenta;
        this.nickName = nickName;
    }

    public Persona getPersona() {
        return persona;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public NickName getNickName() {
        return nickName;
    }
}
