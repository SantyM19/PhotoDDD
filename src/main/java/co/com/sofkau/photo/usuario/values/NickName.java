package co.com.sofkau.photo.usuario.values;

import co.com.sofka.domain.generic.ValueObject;

import java.util.Objects;

public class NickName implements ValueObject<String> {


    private final String value;

    public NickName(String value) {
        this.value = Objects.requireNonNull(value);
        if(this.value.isBlank()){
            throw new IllegalArgumentException("El NickName no puede estar vacio");
        }
        if(value.length() > 60){
            throw new IllegalArgumentException("No puede superar el NickName mas de 60 caracteres");
        }
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NickName nickName = (NickName) o;
        return Objects.equals(value, nickName.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
