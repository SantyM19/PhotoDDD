package co.com.sofkau.photo.usuario;

import co.com.sofka.domain.generic.EventChange;
import co.com.sofkau.photo.usuario.events.*;
import co.com.sofkau.photo.usuario.values.NickName;
import co.com.sofkau.photo.usuario.values.PersonaId;
import co.com.sofkau.photo.usuario.values.UsuarioId;

public class UsuarioChange extends EventChange {
    public UsuarioChange(Usuario usuario) {

        apply((UsuarioCreado event)->{
            usuario.nickName = event.getNickName();
            usuario.persona = event.getPersona();
            usuario.cuenta = event.getCuenta();
        });

        apply((NombrePersonaModificado event)->{
            usuario.persona.modificaNombre(event.name());
        });

        apply((EdadPersonaModificado event)->{
            usuario.persona.modificarEdad(event.edad());
        });

        apply((EmailCuentaModificado event)->{
            usuario.cuenta.modificaEmail(event.email());
        });

        apply((NickNameCambiado event)->{
            usuario.cambiarNickname(event.nickName());
            //usuario.nickName = event.nickName();
        });

    }
}
