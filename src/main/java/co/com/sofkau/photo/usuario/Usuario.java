package co.com.sofkau.photo.usuario;

import co.com.sofka.domain.generic.AggregateEvent;
import co.com.sofka.domain.generic.DomainEvent;
import co.com.sofkau.photo.usuario.events.*;
import co.com.sofkau.photo.usuario.values.*;

import java.util.List;
import java.util.Objects;

public class Usuario extends AggregateEvent<UsuarioId> {
    protected NickName nickName;
    protected Persona persona;
    protected Cuenta cuenta;

    public Usuario(UsuarioId entityId, Persona persona, Cuenta cuenta, NickName nickname) {

        super(entityId);
        appendChange(new UsuarioCreado(persona, cuenta, nickName)).apply();
    }

    private Usuario (UsuarioId entityId){
        super(entityId);
        subscribe(new UsuarioChange(this));
    }

    public static Usuario from(UsuarioId id, List<DomainEvent> retrieveEvents) {
        var usuario = new Usuario(id);
        retrieveEvents.forEach(usuario::applyEvent);
        return usuario;
    }

    public void cambiarNickname(NickName nickName){
        Objects.requireNonNull(nickName);
        appendChange(new NickNameCambiado(nickName)).apply();
    }

    public void modificarNombrePersona(PersonaId entityId, Nombre name){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(name);
        appendChange(new NombrePersonaModificado(entityId, name)).apply();
    }

    public void modificarEdadPersona(PersonaId entityId, Edad edad){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(edad);
        appendChange(new EdadPersonaModificado(entityId, edad)).apply();
    }

    public void modificarEmailCuenta(CuentaId entityId, Email email){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(email);
        appendChange(new EmailCuentaModificado(entityId, email)).apply();
    }

    public NickName nickName() {
        return nickName;
    }

    public Persona persona() {
        return persona;
    }

    public Cuenta cuenta() {
        return cuenta;
    }
}
